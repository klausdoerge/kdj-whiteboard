﻿using Whiteboard.BL.Models.Base;

namespace Whiteboard.BL.Models.Posts
{
    public class PostType : BaseEntity
    {
        public string Name { get; set; }        
    }
}
