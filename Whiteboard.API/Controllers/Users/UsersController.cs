﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Whiteboard.API.Models.Users;
using Whiteboard.API.Services;
using Whiteboard.BL.Models.Users;
using Whiteboard.Data;

namespace Whiteboard.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IUsersService _userService;

        public UsersController(ApplicationDbContext context, IUsersService userService)
        {
            _context = context;
            _userService = userService;
        }

        [HttpGet("me")]
        public async Task<SimpleUser> Me()
        {            
            var myMail = User.FindFirstValue(ClaimTypes.Email);
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == myMail);
            if (user != null)
            {
                return new SimpleUser { Id = user.Id, Name = user.Name, Email = user.Email, Role = user.RoleId };
            }
            else
                return null;
        }


        [AllowAnonymous]
        [HttpPost("createuser")]
        public async Task<ActionResult<User>> CreateUser(CreateUserModel userModel)
        {
            var result = await _userService.CreateUser(userModel);
            if (result > 0)
                return CreatedAtAction("CreateUser", new { content = "User created"});
            else
            {
                return NotFound("User could not be created");
            }
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(SigninUserModel userModel)
        {
            var result = await _userService.Authenticate(userModel);
            if (result.Success)
            {
                return Ok(result.Token);
            }
            else
            {
                return NotFound();
            }
        }


    }
}
