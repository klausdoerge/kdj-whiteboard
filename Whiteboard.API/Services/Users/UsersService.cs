﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;

using System.Linq;


using System.Threading.Tasks;
using Whiteboard.API.Models.Users;
using Whiteboard.API.Services.Authentication;
using Whiteboard.BL.Models.Users;
using Whiteboard.Data;

namespace Whiteboard.API.Services
{
    public class UsersService : IUsersService
    {

        private readonly ISecurityService _securityService;
        private readonly ApplicationDbContext _context;
        
        public UsersService(ApplicationDbContext context, ISecurityService securityService)
        {
            _context = context;
            _securityService = securityService;
        }

        public async Task<AuthenticationModel> Authenticate(SigninUserModel userdata)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == userdata.Email);
            if (user == null)
                return new AuthenticationModel { Message = "Sign-in failed", Success = false };

            var passwordcheck = _securityService.VerifyPassword(userdata.Password, user.Password);

            if (passwordcheck)
            {
                var myToken = _securityService.GenerateToken(user.Email);                
                return new AuthenticationModel { Success = true, Token = myToken };
            }
            else
            {
                return new AuthenticationModel { Message = "Sign-in failed", Success = false };
            }
        }

        public async Task<int> CreateUser(CreateUserModel userModel)
        {
            var userExist = _context.Users.Any(u => u.Email == userModel.Email);
            if (userExist)
            {
                return -1;
            }
            
            var role = _context.UserRoles.FirstOrDefault(r => r.Id == userModel.RoleId);

            if (role != null)
            {
                var user = new User() { 
                    Name = userModel.Name, 
                    Email = userModel.Email, 
                    Password = _securityService.HashPassword(userModel.Password), 
                    Role = role 
                };
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
                return user.Id;
            }
            else
            {
                return -1;
            }

        }
    }
}
