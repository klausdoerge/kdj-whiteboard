﻿using Whiteboard.BL.Models.Base;

namespace Whiteboard.BL.Models.Users
{
    public class UserRole : BaseEntity
    {
        public string Name { get; set; }
    }
}
