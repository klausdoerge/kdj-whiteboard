﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.API.Models.Users;

namespace Whiteboard.API.Services
{
    public interface IUsersService
    {
        Task<AuthenticationModel> Authenticate(SigninUserModel userModel);
        Task<int> CreateUser(CreateUserModel userModel);
    }
}
