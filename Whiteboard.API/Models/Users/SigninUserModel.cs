﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.API.Models.Users
{
    public class SigninUserModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
