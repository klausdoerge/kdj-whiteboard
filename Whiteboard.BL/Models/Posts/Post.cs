﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Whiteboard.BL.Models.Base;
using Whiteboard.BL.Models.Users;

namespace Whiteboard.BL.Models.Posts
{
    public class Post : BaseEntity
    {
        public Post()
        {
            CreateDate = DateTime.Now;
        }

        public string Title { get; set; }
        public string Content { get; set; }

        public string MediaUrl { get; set; }
        public DateTime CreateDate { get; set; }

        public int Likes { get; set; }

        [ForeignKey("PostType")]
        public int PostTypeId { get; set; }
        [JsonIgnore]
        public PostType Type { get; set; }
       
        public bool IsAnonymous { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }
    }
}
