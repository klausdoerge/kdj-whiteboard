﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Whiteboard.BL.Models.Base;
using Whiteboard.BL.Models.Posts;

namespace Whiteboard.BL.Models.Users
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        
        [JsonIgnore]
        public string Password { get; set; }

        [ForeignKey("UserRole")]
        public int RoleId { get; set; }
        public UserRole Role { get; set; }

        public IEnumerable<Post> Posts { get; set; }
    }
}
