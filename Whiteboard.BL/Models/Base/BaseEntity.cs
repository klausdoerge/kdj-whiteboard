﻿using System.ComponentModel.DataAnnotations;

namespace Whiteboard.BL.Models.Base
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
