﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.BL.Models.Posts;

namespace Whiteboard.API.Services.Posts
{
    public interface IPostsService
    {
        Task<IEnumerable<PostType>> ListAvailaPostTypes();
        Task<bool> CreatePost(Post post);        
        Task<IEnumerable<Post>> ListPosts();
    }
}
