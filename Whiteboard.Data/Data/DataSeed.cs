﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whiteboard.BL.Models.Posts;
using Whiteboard.BL.Models.Users;

namespace Whiteboard.Data.Data
{
    public class DataSeed
    {
        public static void Initialize(ApplicationDbContext context)
        {
            //Set standard user-roles
            if (!context.UserRoles.Any())
            {
                var roles = new List<UserRole>()
                {
                    new UserRole { /*Id = 1, */ Name = "User"},
                    new UserRole { /*Id = 2, */ Name ="Moderator"}
                };

                context.UserRoles.AddRange(roles);
                context.SaveChanges();
            }

            //Set Allowed Post Types
            if (!context.PostTypes.Any())
            {
                var postTypes = new List<PostType>()
                {
                    new PostType { /*Id = 1, */ Name = "Text"},
                    new PostType { /*Id = 1, */ Name = "ImageLink"},
                    new PostType { /*Id = 1, */ Name = "Youtube"}                    
                };

                context.PostTypes.AddRange(postTypes);
                context.SaveChanges();
            }
        }
    }
}
