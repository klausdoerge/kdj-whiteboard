﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.API.Services.Posts;
using Whiteboard.BL.Models.Posts;
using Whiteboard.Data;

namespace Whiteboard.API.Controllers.Posts
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {        
        private readonly IPostsService _postsService;

        public PostsController(IPostsService postsService)
        {            
            _postsService = postsService;
        }

        [HttpPost("createpost")]
        public async Task<IActionResult> CreatePost([FromBody] Post post)
        {
            if (await _postsService.CreatePost(post))
                return Ok();
            else
                return BadRequest();
        }

       
        [HttpGet("list")]
        public async Task<IEnumerable<Post>> ListAllPosts()
        {
            return await _postsService.ListPosts();
        }

        [HttpGet("list-posttypes")]
        public async Task<IEnumerable<PostType>> GetPostTypes()
        {
            return await _postsService.ListAvailaPostTypes();
        }
    }
}
