﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.API.Services.Authentication
{
    public interface ISecurityService
    {
        object GenerateToken(string email);
        string HashPassword(string password);

        bool VerifyPassword(string password, string hash);        
    }
}
