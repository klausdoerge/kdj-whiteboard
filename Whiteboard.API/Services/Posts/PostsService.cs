﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.BL.Models.Posts;
using Whiteboard.Data;

namespace Whiteboard.API.Services.Posts
{
    public class PostsService : IPostsService
    {
        private readonly ApplicationDbContext _context;

        public PostsService(ApplicationDbContext context)
        {
            _context = context;        
        }

        public async Task<bool> CreatePost(Post post)
        {
            try
            {
                await _context.Posts.AddAsync(post);
                await _context.SaveChangesAsync();
                return true;
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message?.ToString());
                return false;
            }
            
        }

       
        public async Task<IEnumerable<PostType>> ListAvailaPostTypes()
        {
            return await _context.PostTypes.ToListAsync();
        }


        public async Task<IEnumerable<Post>> ListPosts()
        {
            return await _context.Posts.ToListAsync();
        }
    }
}
